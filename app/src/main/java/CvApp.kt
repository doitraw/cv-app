package com.example.cvapp

import android.app.Application
import com.example.cvapp.com.example.cvapp.di.adapterModule
import com.example.cvapp.com.example.cvapp.di.dataModule
import com.example.cvapp.di.viewModelModule
import com.example.networking.networkingModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CvApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CvApp)
            modules(
                listOf(
                    dataModule,
                    adapterModule,
                    viewModelModule,
                    networkingModule
                )
            )
        }
    }
}