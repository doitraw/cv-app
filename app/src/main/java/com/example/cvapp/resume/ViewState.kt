package com.example.cvapp.resume

import com.example.cvapp.data.presentation.ResumeItem

sealed class ViewState {
    object Loading : ViewState()
    object Refreshing : ViewState()
    object Error : ViewState()
    data class Loaded(val items: List<ResumeItem>) : ViewState()
}
