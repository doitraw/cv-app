package com.example.cvapp.resume

sealed class UserAction
object Load : UserAction()
object Refresh : UserAction()