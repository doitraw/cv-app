package com.example.cvapp.resume

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapp.R
import com.example.cvapp.com.example.cvapp.common.CVAdapter
import com.example.cvapp.extensions.switchVisibility
import kotlinx.android.synthetic.main.fragment_resume.*
import kotlinx.android.synthetic.main.fragment_resume.view.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ResumeFragment : Fragment() {

    private val viewModel: ResumeViewModel by viewModel()

    private val cvAdapter: CVAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_resume, container, false).apply {
        viewModel.apply {
            viewState.observe(viewLifecycleOwner, Observer { renderViewState(it) })
        }
        dataRv.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = cvAdapter
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.refresh.setOnRefreshListener { viewModel.onUserAction(Refresh) }
    }

    private fun renderViewState(state: ViewState) {
        refresh.isRefreshing = state == ViewState.Refreshing
        progressBar.switchVisibility(state == ViewState.Loading)
        errorTv.switchVisibility(state == ViewState.Error)
        dataRv.switchVisibility(state != ViewState.Error)
        if (state is ViewState.Loaded) cvAdapter.submitList(state.items)
    }
}
