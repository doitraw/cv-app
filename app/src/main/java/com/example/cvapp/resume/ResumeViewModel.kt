package com.example.cvapp.resume

import androidx.lifecycle.*
import com.example.cvapp.data.DataRepository
import com.example.cvapp.data.Result

class ResumeViewModel(private val repository: DataRepository) : ViewModel() {

    private val userActions: MutableLiveData<UserAction> = MutableLiveData()

    val viewState: LiveData<ViewState> = userActions.switchMap { action ->
        liveData {
            emit(ViewState.Loading.takeIf { action is Load } ?: ViewState.Refreshing)
            repository.getResume().let {
                emit(
                    when (it) {
                        is Result.Success -> ViewState.Loaded(it.value)
                        is Result.Error -> ViewState.Error
                    }
                )

            }
        }
    }

    init {
        userActions.postValue(Load)
    }

    fun onUserAction(userAction: UserAction) {
        userActions.postValue(userAction)
    }
}