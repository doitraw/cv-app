package com.example.cvapp.common

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class AdapterPlugin {
    abstract fun isForViewType(item: AdapterItem): Boolean
    abstract fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
    abstract fun onBindViewHolder(item: AdapterItem, viewHolder: RecyclerView.ViewHolder)
}