package com.example.cvapp.common

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

open class AdapterPluginManager : ListAdapter<AdapterItem, RecyclerView.ViewHolder>(DiffCallback()) {

    private val plugins: MutableList<AdapterPlugin> = mutableListOf()

    protected fun addPlugin(plugin: AdapterPlugin) {
        plugins.add(plugin)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        plugins[getItemViewType(position)].onBindViewHolder(getItem(position), holder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        plugins[viewType].onCreateViewHolder(parent)

    override fun getItemViewType(position: Int): Int = plugins.indexOfFirst { it.isForViewType(getItem(position)) }

    class DiffCallback : DiffUtil.ItemCallback<AdapterItem>() {
        override fun areItemsTheSame(oldItem: AdapterItem, newItem: AdapterItem) = oldItem::class == newItem::class
        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: AdapterItem, newItem: AdapterItem) = oldItem == newItem
    }
}