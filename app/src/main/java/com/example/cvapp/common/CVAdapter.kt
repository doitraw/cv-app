package com.example.cvapp.com.example.cvapp.common

import com.example.cvapp.com.example.cvapp.plugins.*
import com.example.cvapp.common.AdapterPluginManager

class CVAdapter : AdapterPluginManager() {
    init {
        addPlugin(DefaultItemsPlugin())
        addPlugin(LabelItemPlugin())
        addPlugin(NameItemPlugin())
        addPlugin(ContactItemsPlugin())
        addPlugin(HistoryItemPlugin())
        addPlugin(SkillItemPlugin())
    }
}