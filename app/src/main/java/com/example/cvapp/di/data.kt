package com.example.cvapp.com.example.cvapp.di

import com.example.cvapp.data.DataRepository
import com.example.cvapp.data.GistDataRepository
import org.koin.dsl.module

val dataModule = module {
    single<DataRepository> { GistDataRepository(get()) }
}