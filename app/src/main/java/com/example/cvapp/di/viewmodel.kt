package com.example.cvapp.di

import com.example.cvapp.resume.ResumeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { ResumeViewModel(get()) }
}