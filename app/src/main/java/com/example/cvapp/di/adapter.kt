package com.example.cvapp.com.example.cvapp.di

import com.example.cvapp.com.example.cvapp.common.CVAdapter
import org.koin.dsl.module

val adapterModule = module {
    factory { CVAdapter() }
}