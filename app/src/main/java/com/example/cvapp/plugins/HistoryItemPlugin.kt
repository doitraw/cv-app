package com.example.cvapp.com.example.cvapp.plugins

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapp.R
import com.example.cvapp.common.AdapterItem
import com.example.cvapp.common.AdapterPlugin
import com.example.cvapp.data.presentation.HistoryItem
import com.example.cvapp.extensions.loadImage
import kotlinx.android.synthetic.main.item_contact.view.content
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryItemPlugin : AdapterPlugin() {
    override fun onCreateViewHolder(parent: ViewGroup) = Holder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_history,
            parent,
            false
        )
    )

    override fun onBindViewHolder(item: AdapterItem, viewHolder: RecyclerView.ViewHolder) =
        (viewHolder as Holder).bind(item as HistoryItem)

    override fun isForViewType(item: AdapterItem): Boolean = item is HistoryItem

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: HistoryItem) {
            itemView.apply {
                item.run {
                    type.text = item.position
                    content.text = item.companyName
                    companyLogoIv.loadImage(imageUrl)
                    datesTv.text = context.getString(
                        R.string.dates,
                        fromDate,
                        if (current) context.getString(R.string.current) else toDate.orEmpty()
                    )
                    description.text = additionalText
                }
            }
        }
    }
}