package com.example.cvapp.com.example.cvapp.plugins

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapp.R
import com.example.cvapp.common.AdapterItem
import com.example.cvapp.common.AdapterPlugin
import com.example.cvapp.data.presentation.NameItem
import kotlinx.android.synthetic.main.item_name.view.*

class NameItemPlugin : AdapterPlugin() {
    override fun onCreateViewHolder(parent: ViewGroup) = Holder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_name,
            parent,
            false
        )
    )

    override fun onBindViewHolder(item: AdapterItem, viewHolder: RecyclerView.ViewHolder) =
        (viewHolder as Holder).bind(item as NameItem)

    override fun isForViewType(item: AdapterItem): Boolean = item is NameItem

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: NameItem) {
            itemView.apply {
                item.run {
                    this@apply.content.text = item.name
                }
            }
        }
    }
}