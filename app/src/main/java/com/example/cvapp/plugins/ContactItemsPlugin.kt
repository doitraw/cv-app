package com.example.cvapp.com.example.cvapp.plugins

import android.content.ActivityNotFoundException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapp.R
import com.example.cvapp.common.AdapterItem
import com.example.cvapp.common.AdapterPlugin
import com.example.cvapp.data.presentation.ContactItem
import com.example.cvapp.extensions.loadImage
import com.example.cvapp.extensions.toIntent
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.item_contact.view.*

class ContactItemsPlugin : AdapterPlugin() {
    override fun onCreateViewHolder(parent: ViewGroup) = Holder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_contact,
            parent,
            false
        )
    )

    override fun onBindViewHolder(item: AdapterItem, viewHolder: RecyclerView.ViewHolder) =
        (viewHolder as Holder).bind(item as ContactItem)

    override fun isForViewType(item: AdapterItem): Boolean = item is ContactItem

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: ContactItem) {
            itemView.apply {
                item.run {
                    setOnClickListener {
                        toIntent()?.let {
                            try {
                                context.startActivity(it)
                            } catch (e: ActivityNotFoundException) {
                                Snackbar.make(
                                    itemView,
                                    context.getString(R.string.oops),
                                    Snackbar.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                    contactType.loadImage(
                        when (type) {
                            ContactItem.ContactType.EMAIL -> R.drawable.ic_email_black_24dp
                            ContactItem.ContactType.PHONE -> R.drawable.ic_phone_black_24dp
                            ContactItem.ContactType.WEB -> R.drawable.ic_web_black_24dp
                            ContactItem.ContactType.NONE -> null
                        }
                    )
                    content.text = item.value
                }
            }
        }
    }
}