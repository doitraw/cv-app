package com.example.cvapp.com.example.cvapp.plugins

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cvapp.R
import com.example.cvapp.common.AdapterItem
import com.example.cvapp.common.AdapterPlugin
import com.example.cvapp.data.presentation.SkillItem
import kotlinx.android.synthetic.main.item_contact.view.content
import kotlinx.android.synthetic.main.item_history.view.*

class SkillItemPlugin : AdapterPlugin() {
    override fun onCreateViewHolder(parent: ViewGroup) = Holder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_skill,
            parent,
            false
        )
    )

    override fun onBindViewHolder(item: AdapterItem, viewHolder: RecyclerView.ViewHolder) =
        (viewHolder as Holder).bind(item as SkillItem)

    override fun isForViewType(item: AdapterItem): Boolean = item is SkillItem

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: SkillItem) {
            itemView.apply {
                item.run {
                    this@apply.type.text = item.category
                    this@apply.content.text = item.content
                }
            }
        }
    }
}