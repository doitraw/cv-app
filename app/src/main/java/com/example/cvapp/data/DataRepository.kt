package com.example.cvapp.data

import com.example.cvapp.data.presentation.ResumeItem
import com.example.networking.DataSource
import timber.log.Timber
import java.io.IOException

interface DataRepository {
    suspend fun getResume(): Result<List<ResumeItem>>
}

class GistDataRepository(
    private val dataSource: DataSource
) : DataRepository {
    override suspend fun getResume(): Result<List<ResumeItem>> = dataSource.safeCall(
        call = { getGist() },
        parserMethod = { response -> response.toPresentationModel() },
        onSuccess = { Timber.d("Response is $it") }
    )

    private inline fun <T, R> DataSource.safeCall(
        call: DataSource.() -> T,
        parserMethod: (T) -> R,
        onSuccess: (T) -> Unit = {},
        onError: () -> Unit = {}
    ): Result<R> =
        try {
            Result.Success(parserMethod(call().also { onSuccess(it) }))
        } catch (e: IOException) {
            onError()
            Timber.e(e)
            Result.Error()
        }
}
