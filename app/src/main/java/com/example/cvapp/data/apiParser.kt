package com.example.cvapp.data

import android.util.Patterns
import com.example.cvapp.data.presentation.*
import com.example.networking.model.ResumeContentResponse
import com.example.networking.model.ResumeDataResponse
import com.example.networking.model.ResumeResponse

const val CONTACT_INFO = "Contact Info"
const val PROFESSIONAL_EXPERIENCE = "Professional Experience"
const val TECHNICAL_SKILLS = "Technical Skills"

fun ResumeResponse.toPresentationModel(): List<ResumeItem> = mutableListOf<ResumeItem>().apply {
    name?.let { add(NameItem(name = it)) }
    content?.let { list -> addAll(list.flatMap { it.toPresentationModel() }) }
}

fun ResumeContentResponse.toPresentationModel(): List<ResumeItem> =
    this.data?.map { it.toPresentationModel(label.orEmpty()) }?.toMutableList()?.also {
        it.add(
            0,
            LabelItem(label.orEmpty())
        )
    } ?: listOf()

fun ResumeDataResponse.toPresentationModel(label: String): ResumeItem =
    when (label) {
        CONTACT_INFO -> ContactItem(
            type = when {
                Patterns.PHONE.matcher(secondaryText).matches() -> ContactItem.ContactType.PHONE
                Patterns.EMAIL_ADDRESS.matcher(secondaryText).matches() -> ContactItem.ContactType.EMAIL
                Patterns.WEB_URL.matcher(secondaryText).matches() -> ContactItem.ContactType.WEB
                else -> ContactItem.ContactType.NONE
            },
            value = secondaryText.orEmpty()
        )
        PROFESSIONAL_EXPERIENCE -> HistoryItem(
            position = mainText.orEmpty(),
            companyName = secondaryText.orEmpty(),
            additionalText = optionalText.orEmpty(),
            fromDate = fromDate.orEmpty(),
            toDate = toDate,
            current = isCurrent ?: false,
            imageUrl = imageResource.orEmpty()
        )
        TECHNICAL_SKILLS -> SkillItem(
            category = mainText.orEmpty(),
            content = secondaryText.orEmpty()
        )
        else -> DefaultItem(mainText = mainText.orEmpty(), secondaryText = secondaryText.orEmpty())
    }


