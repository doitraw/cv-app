package com.example.cvapp.data.presentation

import com.example.cvapp.common.AdapterItem

sealed class ResumeItem : AdapterItem

data class NameItem(val name: String) : ResumeItem()

data class LabelItem(val name: String) : ResumeItem()

data class ContactItem(
    val type: ContactType,
    val value: String
) : ResumeItem() {
    enum class ContactType {
        NONE,
        EMAIL,
        PHONE,
        WEB
    }
}

data class HistoryItem(
    val position: String,
    val companyName: String,
    val additionalText: String,
    val imageUrl: String,
    val fromDate: String,
    val toDate: String?,
    val current: Boolean
) : ResumeItem()

data class SkillItem(
    val category: String,
    val content: String
) : ResumeItem()

data class DefaultItem(
    val mainText: String,
    val secondaryText: String
) : ResumeItem()