package com.example.cvapp.extensions

import android.content.Intent
import android.net.Uri
import com.example.cvapp.data.presentation.ContactItem

fun ContactItem.toIntent(): Intent? =
    when (this.type) {
        ContactItem.ContactType.EMAIL -> {
            Intent(Intent.ACTION_SEND).putExtra(Intent.EXTRA_EMAIL, value)
        }
        ContactItem.ContactType.PHONE -> {
            Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:$value"))
        }
        ContactItem.ContactType.WEB -> {
            Intent(Intent.ACTION_VIEW).setData(Uri.parse(value))
        }
        else -> null
    }