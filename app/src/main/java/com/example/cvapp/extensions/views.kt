package com.example.cvapp.extensions

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

fun View.switchVisibility(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

fun ImageView.loadImage(url: String) =
    Glide.with(this)
        .load(url)
        .into(this)

fun ImageView.loadImage(id: Int?) =
    Glide.with(this)
        .load(id)
        .into(this)