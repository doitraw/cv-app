package com.example.cvapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.cvapp.data.DataRepository
import com.example.cvapp.data.GistDataRepository
import com.example.cvapp.data.Result
import com.example.cvapp.data.toPresentationModel
import com.example.cvapp.rules.CoroutinesRule
import com.example.networking.DataSource
import com.example.networking.model.ResumeResponse
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException

@ExperimentalCoroutinesApi
@Suppress("BlockingMethodInNonBlockingContext")
class DataRepositoryTest {

//region mocks

    private val mockDataSource = mock<DataSource> {
        onBlocking { getGist() } doReturn mockResponse
    }

    private val mockResponse = ResumeResponse(
        content = listOf(),
        name = "Artek!"
    )

//endregion

    @get:Rule
    val coroutineRule = CoroutinesRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var repository: DataRepository

    @Before
    fun setUp() {
        repository = GistDataRepository(mockDataSource)
    }

    @Test
    fun `on Success from dataSource repository returns Success with data`() = runBlockingTest {
        whenever(mockDataSource.getGist()).thenReturn(mockResponse)
        Assert.assertEquals(
            repository.getResume(),
            Result.Success(mockResponse.toPresentationModel())
        )
        verify(mockDataSource).getGist()
    }

    @Test
    fun `on Error from dataSource repository returns Error`() = runBlockingTest {
        whenever(mockDataSource.getGist()).thenThrow(IOException())
        Assert.assertTrue(repository.getResume() is Result.Error)
        verify(mockDataSource).getGist()
    }

}
