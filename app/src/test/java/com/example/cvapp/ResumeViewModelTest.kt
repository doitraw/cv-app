package com.example.cvapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.cvapp.data.DataRepository
import com.example.cvapp.data.Result
import com.example.cvapp.data.presentation.ResumeItem
import com.example.cvapp.resume.Refresh
import com.example.cvapp.resume.ResumeViewModel
import com.example.cvapp.resume.ViewState
import com.example.cvapp.rules.CoroutinesRule
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ResumeViewModelTest {

    //region mocks

    private val mockRepository = mock<DataRepository> {
        onBlocking { getResume() } doReturn Result.Error()
    }
    private val mockObserver = mock<Observer<ViewState>> {}

    private val resumeItems = mock<List<ResumeItem>> {}

    //endregion

    @get:Rule
    val coroutineRule = CoroutinesRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: ResumeViewModel

    @Before
    fun setUp() {
        viewModel = ResumeViewModel(mockRepository).apply {
            viewState.observeForever(mockObserver)
        }
    }

    @Test
    fun `when viewModel inits viewModel emits Loading`() = runBlockingTest {
        verify(mockObserver).onChanged(ViewState.Loading)
    }

    @Test
    fun `when refreshed viewModel emits Refreshing`() = runBlockingTest {
        viewModel.onUserAction(Refresh)
        verify(mockObserver).onChanged(ViewState.Refreshing)
    }

    @Test
    fun `when repository returns Error viewModel emits Error`() = runBlockingTest {
        whenever(mockRepository.getResume()).thenReturn(Result.Error())
        verify(mockObserver).onChanged(ViewState.Error)
    }

    @Test
    fun `when repository returns Data viewModel emits Loaded with Data`() = runBlockingTest {
        whenever(mockRepository.getResume()).thenReturn(Result.Success(resumeItems))
        viewModel.onUserAction(Refresh)
        verify(mockObserver).onChanged(ViewState.Loaded(resumeItems))
    }

}