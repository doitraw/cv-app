package com.example.networking.model

import com.google.gson.annotations.SerializedName

data class ResumeResponse(
    @SerializedName("content")
    val content: List<ResumeContentResponse>?,
    @SerializedName("name")
    val name: String?
)