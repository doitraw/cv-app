package com.example.networking.model

import com.google.gson.annotations.SerializedName

data class ResumeDataResponse(
    @SerializedName("fromDate")
    val fromDate: String?,
    @SerializedName("imageResource")
    val imageResource: String?,
    @SerializedName("isCurrent")
    val isCurrent: Boolean?,
    @SerializedName("mainText")
    val mainText: String?,
    @SerializedName("optionalText")
    val optionalText: String?,
    @SerializedName("secondaryText")
    val secondaryText: String?,
    @SerializedName("toDate")
    val toDate: String?
)