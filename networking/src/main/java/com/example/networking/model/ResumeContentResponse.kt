package com.example.networking.model

import com.google.gson.annotations.SerializedName

data class ResumeContentResponse(
    @SerializedName("data")
    val data: List<ResumeDataResponse>?,
    @SerializedName("label")
    val label: String?
)