package com.example.networking

import com.example.networking.model.ResumeResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface GistApi {

    @GET
    suspend fun getGistById(@Url url: String = BuildConfig.CV_PATH): Response<ResumeResponse>
}