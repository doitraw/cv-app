package com.example.networking

import com.example.networking.model.ResumeResponse
import java.io.IOException

interface DataSource {
    @Throws(IOException::class)
    suspend fun getGist(): ResumeResponse
}

class RemoteDataSource(
    private val gistApi: GistApi
) : DataSource {

    override suspend fun getGist() =
        gistApi.getGistById().takeIf { it.isSuccessful }?.body() ?: throw IOException()
}